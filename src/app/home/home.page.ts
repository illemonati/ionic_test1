import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
// import { SMS } from '@ionic-native/sms';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
// import { SMS } from '@ionic-native/sms/ngx';
import { Platform } from '@ionic/angular';
import { Events } from '@ionic/angular';
declare var SMS: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})



export class HomePage {
  constructor(public platform:Platform, public alertController: AlertController, public androidPermissions: AndroidPermissions, private backgroundMode: BackgroundMode, public events: Events) {
    document.addEventListener('onSMSArrive', async (e) => {
      await this.handleSMSRecive(e);
    });
  }

// Things from the html
  newReplyMsg: string;
  startListening: boolean;
  replyToSms: boolean;
  bgm: boolean;
// End Things from the html

  replyMsg = "AUTOREPLY: THIS NUMBER DOES EXIST!";

  async changeMessageWithButton(){
    this.changeReturnMessage(this.newReplyMsg);
  }

  async changeReturnMessage(msg: string){
    try{
      this.replyMsg = msg;
      await this.niceAlert(`Message Succefully changed to ${msg}`);
    }catch(e){
      await this.niceAlert(e);
    }
  }

  async handleSMSRecive(any?: any){
    if (!this.replyToSms) {
      return;
    }
    this.platform.ready().then((readySource) => {
      let filter = {
        box : 'inbox', // 'inbox' (default), 'sent', 'draft'
        indexFrom : 0, // start from index 0
        maxCount : 2, // count of SMS to return each time
      };

      if(SMS) SMS.listSMS(filter, async (ListSms)=>{
        var sender = ListSms[0]["address"];
        SMS.sendSMS(sender, this.replyMsg);
      },
      async Error=>{
        await this.niceAlert(JSON.stringify(Error));
      });
    });
  }

  async enableBackgroundMode(){
    if(this.bgm){
      this.backgroundMode.enable();
    }else{
      this.backgroundMode.disable();
    }
  }

  async checkPermission() {
    this.androidPermissions
      .checkPermission(this.androidPermissions.PERMISSION.READ_SMS)
      .then(
        success => {
          this.niceAlert("success");
        },
        err =>{
          this.androidPermissions
          .requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
          .then(success=>{
            this.niceAlert("success");
          },
          err=>{
            this.niceAlert("cancelled");
          });
        });

    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  }
  public ReadSMSList = async (thing?: any) => {
    this.platform.ready().then((readySource) => {
      let filter = {
        box : 'inbox', // 'inbox' (default), 'sent', 'draft'
        indexFrom : 0, // start from index 0
        maxCount : 2, // count of SMS to return each time
      };

      if(SMS) SMS.listSMS(filter, async (ListSms)=>{
        await this.presentSMS(ListSms[0]);
      },
      async Error=>{
        this.niceAlert(JSON.stringify(Error));
      });
    });
  }

  async presentSMS(sms_txt: JSON) {
    var sender = sms_txt["address"];
    var date_sent = new Date(0);
    date_sent.setUTCSeconds(sms_txt["date_sent"]);
    var body = sms_txt["body"];
    const alert = await this.alertController.create({
      header: sender,
      subHeader: date_sent.toString(),
      message: body,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['K']
    });
    await alert.present();
  }


  async niceAlert(msg: any) {
    const alert = await this.alertController.create({
      message: msg,
      buttons: ['K']
    });
    await alert.present();
  }

  async listenForSMS(){
    if (this.startListening){
      SMS.startWatch();
    }else{
      SMS.stopWatch();
    }
  }

  async sendSMS(){

  }


}
